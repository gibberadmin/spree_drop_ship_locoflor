class PopulateZipCodes < ActiveRecord::Migration
  def change

    require 'csv'
    CSV.foreach("#{Rails.root}/db/zbp12totals.txt",{:headers => true,:return_headers => true}) do |row|
      Spree::DeliveryZipCode.create(zip: row["zip"], city: row["city"], county: row["cty_name"], state: row["stabbr"])
    end
  end

end
