class CreateDeliveryZipCodes < ActiveRecord::Migration
  def change

    create_table :spree_delivery_zip_codes do |t|
      t.string :zip
      t.string :city
      t.string :county
      t.string :state
      t.timestamps
    end

    create_table :spree_delivery_zip_codes_suppliers, id: false do |t|
      t.belongs_to :supplier
      t.belongs_to :delivery_zip_code
    end
    add_index :spree_delivery_zip_codes_suppliers, :supplier_id, name: "s_id"
    add_index :spree_delivery_zip_codes_suppliers, :delivery_zip_code_id, name: "d_id"

  end

end
