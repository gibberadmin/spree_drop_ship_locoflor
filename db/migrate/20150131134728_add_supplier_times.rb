class AddSupplierTimes < ActiveRecord::Migration
  def change
    add_column :spree_suppliers, :timezone_offset, :integer, :default => 6
    add_column :spree_suppliers, :max_order_lead_time_days, :integer, :default => 14

    %w{monday tuesday wednesday thursday friday saturday sunday}.each do |day|
      add_column :spree_suppliers, :"#{day}_open", :boolean, :default => false
      add_column :spree_suppliers, :"#{day}_pickup_start", :time, :null => true
      add_column :spree_suppliers, :"#{day}_pickup_end", :time, :null => true
      add_column :spree_suppliers, :"#{day}_deliveries_start", :time, :null => true
      add_column :spree_suppliers, :"#{day}_deliveries_end", :time, :null => true
      add_column :spree_suppliers, :"#{day}_lead_time_in_hours", :integer, :default => 24
    end
  end
end
